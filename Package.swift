// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "XIBLodable",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "XIBLodable",
            targets: ["XIBLodable"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        .binaryTarget(name: "XIBLodable", url: "https://cordovasampl.web.app/XIBLoadable.xcframework.zip", checksum: "61658f0681e535a4b5f9a050c9bdb44ed5cea1d26807d0b87083f6698a0b0340")
    ]
)
